function sortedInventory(inventory){
    
    return inventory.sort((a,b)=>{ 
        let name_a = a['car_model'].toUpperCase()
        let name_b = b['car_model'].toUpperCase()
        if(name_a<name_b){ return -1}
        if(name_a>name_b){return 1}
        return 0
    })
}

module.exports = sortedInventory;