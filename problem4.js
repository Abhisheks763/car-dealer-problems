function yearArray(inventory){
    let year_record=[]
    for(let car of inventory){
        year_record.push(car['car_year'])
    }
    return year_record
}

module.exports = yearArray;