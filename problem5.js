function countOfCars(inventory){
    let count = 0;
    for(let car of inventory){
        if(car['car_year']<2000){count++}
    }
    return count
}

module.exports = countOfCars;