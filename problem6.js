function bmwAudi(inventory){
    let bmwAndAudiCars =[]
    for(let car in inventory){
        if (car['car_make']==='BMW'||car['car_make']==='Audi'){bmwAndAudiCars.push(car)}
    }
    return bmwAndAudiCars
}

module.exports = bmwAudi;